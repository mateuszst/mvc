<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MainController
 *
 * @author diabelek
 */
namespace MVC;

use MVC\Model\Movies;

class MainController extends AbstractController {
    public function renderPage($field = 'name', $order = 'ASC')
    {
        $movies = new Movies();
        
        if (!in_array($order, Movies::$sortingOrder)) {
            $order = 'ASC';
        }
               
        if (!in_array($field, Movies::$sortingFields)) {
            $field = 'name';
        }
        
        return $this->twig->render('main-page.twig', [
            'movies' => $movies->getAllMovies($field, $order),
            'title' => 'STRONA GŁÓWNA'
        ]);
    }
}
