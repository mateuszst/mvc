<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace MVC\Model;

/**
 * Description of Movies
 *
 * @author diabelek
 */
class Movies extends AbstractModel {
    public static $sortingFields = [
        'name',
        'productionYear',
        'duration'
    ];
           
    public static $sortingOrder = [
        'ASC',
        'DESC'
    ];
    
    public function getAllMovies($field, $order)
    {
        $result = $this->conn->query('SELECT m_id as id, m_name as name,'
                . 'm_duration as duration, p_yearp as productionYear FROM movie '
                . 'LEFT JOIN production_year ON m_yearp_id = p_id ORDER BY '
                . $field . ' ' . $order);
        
        return $result->fetch_all(MYSQLI_ASSOC);
    }
}
