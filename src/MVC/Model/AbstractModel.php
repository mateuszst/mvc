<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace MVC\Model;

/**
 * Description of AbstractModel
 *
 * @author diabelek
 */
abstract class AbstractModel {
    protected $conn;
    
    public function __construct() {
        $this->conn = new \mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_DB);
    }
}
